﻿using System;

/// <summary>
/// Решение очередного задания по курсу .net c# EPAM Апрель-Май 2020.
/// Простейшая консольная игра о поле с ловушками, которое необходимо пройти.
/// Решал задание: Захаренко Владимир.
/// </summary>

namespace FinalModuleGame
{
    class Program
    {
        static void Main(string[] args)
        {
            // В ДАННОЙ РЕАЛИЗАЦИИ ПРОГРАММЫ ЛОВУШКИ НЕ ОДНОРАЗОВЫЕ.
            // ЭТО ЗНАЧИТ, ЧТО ЕСЛИ ПОВТОРНО НАСТУПИТЬ НА ЛОВУШКУ, ВЫ СНОВА ПОВРЕДИТЕ ЗДОРОВЬЕ!

            const bool showAllAsDigitField = true;     // Показать всё на поле? Для теста удобен true.
                                                        // При значении true, поле отображается как цифровая матрица.

            const int countOfTrap = 10;                 // Количество ловушек: всего возможно количество не более 98 (10x10 минус 1:1 и 10:10).
                                                        // Рекоммендуется не более 40, т.к. далее задача становится очевидно нереальной.
            
            int[,] field = new int[10, 10];             // Матрица игрового поля.

            int x;                              // Координата X;
            int y;                              // Координата Y;
            int health = 10;                    // Здоровье.

            int choise = 0;                     // Очередной ход игрока.            
            bool isPlayerWin = false;           // Победил игрок?            
            bool playAgain;                     // Повторять игру?

            do
            {
                playAgain = false;
                ClearScore(out x, out y, out health);           // Исходные параметры.
                FillTheField(field, countOfTrap);               // Заполняем поле ловушками.

                GamePlay(field, ref x, ref y,
                    ref health, ref choise,
                    ref isPlayerWin, showAllAsDigitField);           // Играем игру.

                ShowTheField(field, x, y, showAllAsDigitField);      // Показываем поле.
                ShowResultOfGame(isPlayerWin);                  // Показываем результат игры.

                playAgain = WillWePlayAgain(playAgain);         // Играем еще?

            } while (playAgain);
        }

        // Опрос на еще один сеанс игры.
        private static bool WillWePlayAgain(bool playAgain)
        {
            Console.WriteLine("Желаете сыграть ещё раз?");
            Console.WriteLine("Введите \"Да\" или \"Yes\", если хотите сыграть снова...\n");
            Console.Write(">");
            string answer = Console.ReadLine().Trim().ToLower();
            if (answer.Equals("yes") | answer.Equals("да"))
            {
                playAgain = true;
            }
            return playAgain;
        }

        // Обновить результаты игры.
        private static void ClearScore(out int x, out int y, out int health)
        {
            x = 0;
            y = 0;
            health = 10;
        }

        // Играем игру.
        private static bool GamePlay(int[,] field, ref int x, ref int y, ref int health, ref int choise, ref bool isPlayerWin, bool showAll)
        {
            bool endOfGame;
            do
            {
                ShowTheField(field, x, y, showAll);
                ShowHelthInfo(health);
                ShowMENU();
                ChoiseOfPlayer(ref x, ref y, ref choise);
                endOfGame = ChekingHealthAndFinishPosition(field, x, y, ref health, ref isPlayerWin);
            } while (!endOfGame);
            return endOfGame;
        }

        // Вывод результата игры.
        private static void ShowResultOfGame(bool isPlayerWin)
        {    
            if (isPlayerWin)
            {
                Console.WriteLine("ПРИМИТЕ ПОЗДРАВЛЕНИЯ!!!\nНе смотря на все препятствия, Вы добрались до цели...");                
            }
            else
            {
                Console.WriteLine("ЗДОРОВЬЕ ЗАКОНЧИЛОСЬ!\nК сожалению вы проиграли, т.к. ваше здоровье закончилось!");
            }
        }

        // Проверка конца игры по здоровью. Обновление здоровья.
        private static bool ChekingHealthAndFinishPosition(int[,] field, int x, int y, ref int health, ref bool isPlayerWin)
        {
            if (field[x, y] > 0)
            {
                health -= field[x, y];
            }

            if (health <= 0)
            {
                return true;
            }
            else
            {
                if (x == 9 & y == 9)
                {                    
                    isPlayerWin = true;
                    return true;
                }
            }
            return false;
        }

        // Ход игрока.
        private static bool ChoiseOfPlayer(ref int x, ref int y, ref int choise)
        {
            try
            {
                    choise = Convert.ToInt32(Console.ReadLine().Trim());
            }
            catch (Exception)
            {

                return false;
            }
            

            switch (choise)
            {
                case 1:
                    if (x>0)
                        x--;
                    break;
                case 2:
                    if (x<9)
                    x++;
                    break;
                case 3:
                    if (y>0)
                    y--;
                    break;
                case 4:
                    if (y<9)
                    y++;
                    break;

                default:
                    return true;                    
            }
            return false;
        }

        // Вывод МЕНЮ передвижения.
        private static void ShowMENU()
        {
            Console.WriteLine("Выберите направление хода.");
            Console.WriteLine("\t1. Влево.");
            Console.WriteLine("\t2. Вправо.");
            Console.WriteLine("\t3. Вверх.");
            Console.WriteLine("\t4. Вниз.");
            Console.Write("> ");
        }
        
        // Вывод информации о здоровье в консоль.
        private static void ShowHelthInfo(int health)
        {
            Console.WriteLine("Здоровье:\t{0}0%", health);

            
        }

        // Заполнение игрового поля ловушками.
        private static void FillTheField(int[,] field, int countOfTrap)
        {
            int[,] coordinates = new int[2, countOfTrap];   // Массив уникальных координат каждой ловушки.
            Random random = new Random();

            for (int i = 0; i < countOfTrap; i++)
            {
                int x, y;
                do
                {
                    x = random.Next(10);
                    y = random.Next(10);
                } while ((x == 0 & y == 0) |
                            (x == 9 & y == 9) |
                            CheckCoordinates(x, y, coordinates));
                coordinates[0, i] = x;
                coordinates[1, i] = y;
                
                field[x, y] = random.Next(1, 11);
            }
        }

        // Проверка уникальности координат ловушки.
        private static bool CheckCoordinates(int x, int y, int[,] coordinates)
        {
            for (int i = 0; i < coordinates.GetLength(1); i++)
            {
                if (coordinates[0, i] == x & coordinates[1, i] == y)
                {
                    return true;
                }
            }
            return false;
        }

        //  Вывод игрового поля в консоль.
        private static void ShowTheField(int[,] field, int xPos, int yPos, bool showAllAsDigitField)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("ВНИМАНИЕ: ПОПАДАНИЕ В ЛОВУШКУ (В ТОМ ЧИСЛЕ ПОВТОРНОЕ) ПОВРЕЖДАЕТ ЗДОРОВЬЕ!");
            Console.ForegroundColor = ConsoleColor.Yellow;
            for (int y = 0; y < 10; y++)
            {
                for (int x = 0; x < 10; x++)
                {
                    if (x == xPos & y == yPos)
                    {
                        if (!showAllAsDigitField)
                        {

                            Console.BackgroundColor = ConsoleColor.Red;

                            if (field[x, y] > 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write("{0}  \t", field[x, y]);
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("   \t");                            
                                Console.ForegroundColor = ConsoleColor.Yellow;
                            }
                            Console.BackgroundColor = ConsoleColor.Black;
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.Write("{0}\t", field[x, y]);
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.Yellow;
                        }
                        
                    }
                    else
                    {
                        if ((x == 0 & y == 0) || (x == 9 & y == 9))
                        {
                            if (!showAllAsDigitField)
                            {
                                Console.BackgroundColor = ConsoleColor.Green;
                                Console.Write("   \t");
                                Console.BackgroundColor = ConsoleColor.Black;
                            }
                            else
                            {

                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write("#\t");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                            }
                        }
                        else
                        {
                            if (!showAllAsDigitField)
                            {
                                Console.BackgroundColor = ConsoleColor.DarkGray;
                                Console.Write("   \t");
                                Console.BackgroundColor = ConsoleColor.Black;
                            }
                            else

                                Console.Write("{0}\t", field[x, y]);                            
                        }
                        
                    }
                }
                Console.WriteLine("\n");
            }
            
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
